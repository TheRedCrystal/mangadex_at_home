/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
*/
package mdnet.server

import io.kotest.core.spec.style.FreeSpec
import mdnet.security.TweetNaclFast
import org.http4k.core.*
import org.http4k.kotest.shouldNotHaveStatus
import org.http4k.routing.bind
import org.http4k.routing.routes

class TokenVerifierTest : FreeSpec() {
    init {
        val remoteKeys = TweetNaclFast.Box.keyPair()
        val clientKeys = TweetNaclFast.Box.keyPair()
        val box = TweetNaclFast.Box(clientKeys.publicKey, remoteKeys.secretKey)

        val backend = TokenVerifier(box.before()).then {
            Response(Status.OK)
        }

        val handler = routes(
            "/data/{chapterHash}/{fileName}" bind Method.GET to backend,
            "/data-saver/{chapterHash}/{fileName}" bind Method.GET to backend,
            "/{token}/data/{chapterHash}/{fileName}" bind Method.GET to backend,
            "/{token}/data-saver/{chapterHash}/{fileName}" bind Method.GET to backend,
        )

        "invalid" - {
            "missing token should fail" {
                val response = handler(Request(Method.GET, "/data/02181a8f5fe8cd408720a771dd129fd8/T2.png"))
                response.shouldNotHaveStatus(Status.OK)
            }

            "too short token should fail" {
                val response = handler(Request(Method.GET, "/a/data/02181a8f5fe8cd408720a771dd129fd8/T2.png"))
                response.shouldNotHaveStatus(Status.OK)
            }

            "invalid token should fail" {
                val response = handler(
                    Request(
                        Method.GET,
                        "/MTIzM2Vhd2Z3YWVmbG1pbzJuM29pNG4yaXAzNG1wMSwyWzMscHdxZWVlZWVlZXBscWFkcVt3ZGwxWzJsM3BbMWwycFsxZSxwMVssZmRbcGF3LGZwW2F3ZnBbLA==/data/02181a8f5fe8cd408720a771dd129fd8/T2.png"
                    )
                )
                response.shouldNotHaveStatus(Status.OK)
            }
        }
    }
}
