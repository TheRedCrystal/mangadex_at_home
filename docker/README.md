# Run with Docker

⚠ This is a bit more involved of a setup than just running the jar ⚠

## Prerequisites

Docker Desktop for your operating system.

Once installed, you can check that it works by opening a command prompt and running

    docker run -it hello-world

## Run as a standalone container

Use either a specific image, preferrably the [latest image published](https://gitlab.com/mangadex-pub/mangadex_at_home/container_registry/1200259)

> While it might work, using `registry.gitlab.com/mangadex-pub/mangadex_at_home:latest` is a bad idea as we do not guarantee forward-compatibility

## Run with Prometheus and Grafana (i.e. dashboards)

![](dashboard.png)

### Quickstart

1. Install `docker-compose`. Follow the steps [here](https://docs.docker.com/compose/install/)

2. Copy the `docker` directory somewhere *on the drive you want to use as cache storage**

   a. edit `docker-compose.yml` and replace `registry.gitlab.com/mangadex-pub/mangadex_at_home:<version>` with the appropriate version

3. Copy your `settings.yaml` inside that directory (it should be next to `docker-compose.yml`)

4. Run `docker-compose up -d` from within this directory

5. That's it. You should now check the following:
    - There are 3 containers in 'Up' state when running `docker ps` (`mangadex-at-home`, `prometheus` and `grafana`)
    - The test image loads at [https://localhost/data/8172a46adc798f4f4ace6663322a383e/B18.png](https://localhost/data/8172a46adc798f4f4ace6663322a383e/B18.png)
    - Prometheus loads at [http://localhost:9090](http://localhost:9090)
    - Grafana loads at [http://localhost:3000/dashboards](http://localhost:3000/dashboards) and you can open the dashboard

### Notes

The pre-made configuration is hardcoded both public port 443 and this directory structure:

    <directory where you run 'docker-compose up'>

        Folders/files copied from the git repository
        -> prometheus/... - pre-made config
        -> grafana/... - pre-made config
        -> docker-compose.yml

        Your settings.yaml
        -> settings.yaml

        Created by the containers
        -> data/
            -> cache - the client's image cache
            -> prometheus - prometheus database files
            -> grafana - grafana files

All of this is configurable to suit your needs but is not recommended unless you are familiar with Docker already.
