/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
*/
package mdnet.settings

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import dev.afanasev.sekret.Secret

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy::class)
data class ClientSettings(
    val maxCacheSizeInMebibytes: Long,
    val serverSettings: ServerSettings,

    val devSettings: DevSettings = DevSettings(),
    val metricsSettings: MetricsSettings = MetricsSettings(),
)

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy::class)
data class ServerSettings(
    @field:Secret val secret: String,
    val externalPort: Int = 0,
    val gracefulShutdownWaitSeconds: Int = 60,
    val hostname: String = "0.0.0.0",
    val maxKilobitsPerSecond: Long = 0,
    val externalMaxKilobitsPerSecond: Long = 0,
    val maxMebibytesPerHour: Long = 0,
    val externalIp: String? = null,
    val port: Int = 443,
    val threads: Int = 0,
)

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy::class)
data class DevSettings(
    val devUrl: String? = null
)

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy::class)
data class MetricsSettings(
    val enableGeoip: Boolean = false,
    @field:Secret val geoipLicenseKey: String = "none"
)
